from flask import *

app= Flask (__name__)

@app.route ('/restar', methods= ['POST'])
def restar ():
    misDatos=request.get_json ()

    a=misDatos ["numero1"]
    b=misDatos ["numero2"]
    resultado= a-b 
    return jsonify ({
        "resultado": resultado
    })

@app.route ('/multiplicar', methods= ['POST'])
def multiplicar ():
    miDato= request.get_json ()
    a=miDato ["numero1"]
    b=miDato ["numero2"]
    resultado= a*b 
    return jsonify ({
        "resultado": resultado
    })

@app.route ('/dividir', methods= ['POST'])
def dividir ():
    misDatos= request.get_json ()
    a=misDatos ["numero1"]
    b=misDatos ["numero2"]
    resultado= a/b 
    return jsonify ({
        "resultado": resultado
    })

@app.route ('/modulo', methods= ['POST'])
def modulo ():
    miDato=request.get_json ()
    a=miDato ["numero1"]
    b=miDato ["numero2"]
    resultado= a%b 
    return jsonify ({
        "resultado": resultado
    })

app.run ()